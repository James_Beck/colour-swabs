﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Colour_Swabs
{
    public partial class MainPage : ContentPage
    {
        private FrontPanel FrontPanelContext { get; set; } = new FrontPanel();

        public MainPage()
        {
            InitializeComponent();

            foreach (string x in colorInfo.Keys)
            {
                colourPicker.Items.Add(x);
            }

            BindingContext = FrontPanelContext;
        }

        private void backPanel(object sender, EventArgs e)
        {
            BackPanel1.BackgroundColor = FrontPanelContext.Color;
            BackPanel2.BackgroundColor = FrontPanelContext.Color;
            BackPanel3.BackgroundColor = FrontPanelContext.Color;
            BackPanel4.BackgroundColor = FrontPanelContext.Color;
        }

        private void colorChoice(object sender, EventArgs e)
        {
            var selected = colourPicker.Items[colourPicker.SelectedIndex];

            switch (selected)
            {
                case "Aqua":
                    
                    break;
            }
        }

        Dictionary<string, Color> colorInfo = new Dictionary<string, Color>
        {
            { "Aqua", Color.Aqua },
            { "Black", Color.Black },
            { "Blue", Color.Blue },
            { "Gray", Color.Gray },
            { "Green", Color.Green },
            { "Lime", Color.Lime },
            { "Maroon", Color.Maroon },
            { "Navy", Color.Navy },
            { "Olive", Color.Olive },
            { "Purple", Color.Purple },
            { "Red", Color.Red },
            { "Silver", Color.Silver },
            { "Teal", Color.Teal },
            { "White", Color.White },
            { "Yellow", Color.Yellow },
            { "Fuscia", Color.Fuchsia }
        };
    }

    public class FrontPanel : INotifyPropertyChanged
    {
        int red, green, blue, opacity;
        Color color;

        public event PropertyChangedEventHandler PropertyChanged;

        public int Red
        {
            set
            {
                if (red != value)
                {
                    red = value;
                    OnPropertyChanged("Red");
                    SetNewColor();
                }
            }
            get
            {
                return red;
            }
        }

        public int Green
        {
            set
            {
                if (green != value)
                {
                    green = value;
                    OnPropertyChanged("Green");
                    SetNewColor();
                }
            }
            get
            {
                return green;
            }
        }

        public int Blue
        {
            set
            {
                if (blue != value)
                {
                    blue = value;
                    OnPropertyChanged("Blue");
                    SetNewColor();
                }
            }
            get
            {
                return blue;
            }
        }

        public int Opacity
        {
            set
            {
                if (opacity != value)
                {
                    opacity = value;
                    OnPropertyChanged("Opacity");
                    SetNewColor();
                }
            }
            get
            {
                return opacity;
            }
        }

        public Color Color
        {
            set
            {
                if (color != value)
                {
                    color = value;
                    OnPropertyChanged("Color");
                }
            }
            get
            {
                return color;
            }
        }

        void SetNewColor()
        {
            Color = Color.FromRgba(red, green, blue, opacity);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }    
}
